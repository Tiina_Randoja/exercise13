﻿using System;

namespace calc
{
    class Program
    {
        static void Main(string[] args)
        {//Start the program with Clear();
         var myname = "Tiina";
        Console.Clear();
        
        
        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine("Hi. Would you like to find out my name?. Press <Enter> to continue");
        Console.ReadKey();
       
        Console.WriteLine($"My name is {myname}");
  
        Console.WriteLine("Press <Enter> to exit");
        Console.ReadKey();
       
            
        }
    }
}